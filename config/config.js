/**
 * For development you can set the variables by creating a .env file on the root
 */
var fs = require('fs');
// var production = process.env.NODE_ENV === 'production';
var production = false;

module.exports = {
  "PORT_HTTP_W":  process.env.PORT_HTTP_W || 9999,
  "PORT_HTTPS_W":  process.env.PORT_HTTPS_W || 4430,
  "USE_HTTPS": process.env.USE_HTTPS || true,
  "HTTPS_KEY": process.env.HTTPS_KEY || './ssl/private.key',
  "HTTPS_CERT": process.env.HTTPS_CERT || './ssl/certificate.crt',
  "HTTPS_CA": process.env.HTTPS_CA || './ssl/ca_bundle.crt',
};
