
var assert             = require('better-assert');
var recaptchaValidator = require('recaptcha-validator');
var uuid               = require('uuid');
var fs                 = require('fs');
var parse              = require('csv-parse');
var csv                = require('fast-csv');
var mime               = require('mime');

var trainFlag = {};
var yearCnt = {};
var csvData=[];

var fileForDownload;

module.exports = function(app)
{
    app.get('/' , function(req , res , next){
        initYearCnt();
        res.render('index.html');
    });

    app.get('/result' , function(req, res, next){
        // res.render('result.html' , {url : fileForDownload});

        var file = fileForDownload;

        var mimetype = mime.lookup(file);

        res.setHeader('Content-disposition', 'attachment; filename='+fileForDownload);
        res.setHeader('Content-type', mimetype);

        var filestream = fs.createReadStream(file);

        filestream.pipe(res);
    });
    //     app.get('/get_excel_report', function (req, res, next) {
    //     var file = 'report.xlsx';
    //     var mimetype = mime.lookup(file);

    //     res.setHeader('Content-disposition', 'attachment; filename=report.xlsx');
    //     res.setHeader('Content-type', mimetype);

    //     var filestream = fs.createReadStream(file);
    //     filestream.pipe(res);
    // });

    app.post('/getyear' , function(req , res , next){
        var queryString = req.body.queryString;
        // console.log(queryString);
        res.json(getYear(queryString));
    });

    app.post('/train' , function(req, res, next){
        var queryString = req.body.queryString;
        var from = req.body.from;
        var to = req.body.to;
        // console.log('train data');
        // console.log(from);
        // console.log(to);
        // console.log(queryString);
        res.json(trainWords(queryString, from, to));
    });

    app.post('/init' , function(req, res, next){
        initYearCnt();
        yearCnt['later'][0] = req.body.later?parseInt(req.body.later):8;

        yearCnt['early'][0] = req.body.early?parseInt(req.body.early[0]):0;
        yearCnt['early'][1] = req.body.early?parseInt(req.body.early):19;

        yearCnt['searly'][0] = req.body.searly?parseInt(req.body.searly[0]):0;
        yearCnt['searly'][1] = req.body.searly?parseInt(req.body.searly[1]):2;

        yearCnt['mid'][0] = req.body.mid?parseInt(req.body.mid[0]):40;
        yearCnt['mid'][1] = req.body.mid?parseInt(req.body.mid[1]):59;

        yearCnt['smid'][0] = req.body.smid?parseInt(req.body.smid[0]):4;
        yearCnt['smid'][1] = req.body.smid?parseInt(req.body.smid[1]):6;

        yearCnt['late'][0] = req.body.late?parseInt(req.body.late[0]):80;
        yearCnt['late'][1] = req.body.late?parseInt(req.body.late[1]):99;

        yearCnt['slate'][0] = req.body.slate?parseInt(req.body.slate[0]):7;
        yearCnt['slate'][1] = req.body.slate?parseInt(req.body.slate[1]):9;

        yearCnt['before'][0] = req.body.before?parseInt(req.body.before):20;

        yearCnt['after'][0] = req.body.after?parseInt(req.body.after):20;

        yearCnt['near'][0] = req.body.near?parseInt(req.body.near):10;

        yearCnt['ca'][0] = req.body.ca?parseInt(req.body.ca):5;

        yearCnt['end'][0] = req.body.end?parseInt(req.body.end[0]):90;
        yearCnt['end'][1] = req.body.end?parseInt(req.body.end[1]):99;

        yearCnt['send'][0] = req.body.send?parseInt(req.body.send[0]):8;
        yearCnt['send'][1] = req.body.send?parseInt(req.body.send[1]):9;
        res.json({result: 1});
    });

    app.post('/csvupload' , function(req, res, next){

        let csvupload = req.files.csvupload;

        var str = csvupload.name;
        str = str.replace(" " , "-");
        var newFileName = uuid.v4() + str;

        var fileUploaded = "./uploads/" + newFileName;

        fileForDownload = "./downloads/" + newFileName;

        csvupload.mv(fileUploaded, function(err){
           // console.log(result.status + result.msg);
           if(err)
            {    // res.redirect("/error");
                console.log("error occured");
                res.send(err);
            }
            else
            {
                console.log("successfully uploaded");
                var ws = fs.createWriteStream(fileForDownload);
                fs.createReadStream(fileUploaded)
                    .pipe(parse({delimiter: ','}))
                    .on('data', function(csvrow) {
                        csvData.push(csvrow);
                        // console.log('data');
                        // console.log(csvrow);
                    })
                    .on('end',function() {
                      //do something wiht csvData
                        console.log('end');
                        console.log(csvData.length);
                        var space = " ";
                        var newLine = "\r\n"
                        var record = [];


                        for(var i = 0; i < csvData.length; i++){

                            if(csvData[i].toString() == "") continue;

                            var rlt = getYear(csvData[i].toString());
                            record[i] = [];
                            record[i][0] = csvData[i];
                            record[i][1] = rlt.start;
                            record[i][2] = rlt.end;
                            record[i][3] = newLine;
                        }

                        csv.write(record, {headers : true}).pipe(ws).on('finish' , function(){
                            console.log('end all');
                            res.redirect('/result');
                        });

                    });



            }

        });
    });

}

function initYearCnt(){
    trainFlag['later'] = 0;
    trainFlag['early'] = 0;
    trainFlag['searly'] = 0;
    trainFlag['mid'] = 0;
    trainFlag['smid'] = 0;
    trainFlag['late'] = 0;
    trainFlag['slate'] = 0;
    trainFlag['before'] = 0;
    trainFlag['after'] = 0;
    trainFlag['near'] = 0;
    trainFlag['ca'] = 0;
    trainFlag['end'] = 0;

    yearCnt['later'] = [];
    yearCnt['later'][0] = 8;

    yearCnt['early'] = [];
    yearCnt['early'][0] = 0;
    yearCnt['early'][1] = 19;

    yearCnt['searly'] = [];
    yearCnt['searly'][0] = 0;
    yearCnt['searly'][1] = 2;

    yearCnt['mid'] = [];
    yearCnt['mid'][0] = 40;
    yearCnt['mid'][1] = 59;

    yearCnt['smid'] = [];
    yearCnt['smid'][0] = 4;
    yearCnt['smid'][1] = 6;

    yearCnt['late'] = [];
    yearCnt['late'][0] = 80;
    yearCnt['late'][1] = 99;

    yearCnt['slate'] = [];
    yearCnt['slate'][0] = 7;
    yearCnt['slate'][1] = 9;

    yearCnt['before'] = [];
    yearCnt['before'][0] = 20;

    yearCnt['after'] = [];
    yearCnt['after'][0] = 20;

    yearCnt['near'] = [];
    yearCnt['near'][0] = 10;

    yearCnt['ca'] = [];
    yearCnt['ca'][0] = 5;

    yearCnt['end'] = [];
    yearCnt['end'][0] = 90;
    yearCnt['end'][1] = 99;

    yearCnt['send'] = [];
    yearCnt['send'][0] = 8;
    yearCnt['send'][1] = 9;
}

function onlyNum(str){

    if(str == undefined || str == "") return false;
    for(var i = 0; i < str.length; i++){
        if(str.charAt(i) > '9' || str.charAt(i) < '0') return false;
    }

    return true;
}

function isYearNum(str){

    var numPartStr = "";

    for(var i = 0; i < str.length; i++){
        if(str.charAt(i) > '9' || str.charAt(i) < '0') break;
        numPartStr = numPartStr + str.charAt(i);
    }

    if(numPartStr == "") return 0;
    if(onlyNum(str) == false){
        if(parseInt(numPartStr) < 4){
            return 0;
        }
        else if(parseInt(numPartStr) <= 10){
            return 1;
        }
    }
    return 2;
}

function isDQH(str){
    if(str.toLowerCase() == "decade" ){
        return 0;
    }

    if(str.toLowerCase() == "quarter"){
        return 1;
    }

    if(str.toLowerCase() == "half"){
        return 2;
    }

    if(str.toLowerCase() == "early"){
        return 3;
    }

    if(str.toLowerCase() == "mid"){
        return 4;
    }

    if(str.toLowerCase() == "late"){
        return 5;
    }

    if(str.toLowerCase() == "before"){
        return 6;
    }

    if(str.toLowerCase() == "after"){
        return 7;
    }

    if(str.toLowerCase() == "near"){
        return 8;
    }

    if(str.toLowerCase() == "ca" || str.toLowerCase() == "c"){
        return 9;
    }

    if(str.toLowerCase() == "end"){
        return 10;
    }

    return -1;
}

function getYear(oriStr){

    var startYear = 0;
    var endYear = 0;

    var str = junkCharRemove(oriStr);
    console.log('------------junk char remove------------');
    console.log(str);

    var words = [];
    words = str.split(" ");
    var word_cnt = words.length;

    var ndCnt = 0;
    var desFlag = false;
    var panelFlag = false;

    for(var i = 0; i < word_cnt; i++){
        if(words[i].toLowerCase() == "n"  || words[i].toLowerCase() == "d" ) ndCnt ++;
        if(words[i].toLowerCase() == "description" ) desFlag = true;
        if(words[i].toLowerCase() == "panel" ) panelFlag = true;
    }

    if(ndCnt >= 2 || desFlag == true || panelFlag == true) {

        return {'start' : '' , 'end' : ''};
    }

    var yearNumCnt = 0;
    var yearNumInd = [];

    for (var i = 0; i < word_cnt; i++){
        if(isYearNum(words[i]) > 0){
            if(isYearNum(words[i]) == 1 && (i < (word_cnt - 1)) && isDQH(words[i + 1]) > -1 && isDQH(words[i + 1]) < 3){
                continue;
            }
            yearNumInd[yearNumCnt] = i;
            yearNumCnt ++;
        }
    }

    if(yearNumCnt == 0){
        return {'start' : 0 , 'end' : 0};
    }
    /*
    console.log(yearNumCnt);
    for(var i = 0; i < yearNumCnt; i++){
        console.log(words[yearNumInd[i]]);
    }
    */
    // console.log('words - yearNumInd');
    // console.log(words);
    // console.log(yearNumInd);
    var sYear1 = -1;
    var sYear2 = -1;
    var eYear1 = -1;
    var eYear2 = -1;

    var str_sY = "";
    var str_eY = "";

    str_sY = words[yearNumInd[0]];
    if(onlyNum(str_sY) == true ){
        sYear1 = parseInt(str_sY);
        sYear2 = sYear1;
    }

    // find four-digits number or 2 digits number + 'th' or 4 digits number + "s"

    else if(str_sY.charAt(str_sY.length - 2) == 't' || str_sY.charAt(str_sY.length - 2) == 'T' || str_sY.substring(str_sY.length-2) == 'st' || str_sY.substring(str_sY.length-2) == 'nd' || str_sY.substring(str_sY.length-2) == 'rd'){
        var tenPow = 1;
        sYear2 = 0;
        for(var i = str_sY.length - 3; i >= 0; i--){
            sYear2 = sYear2 + (str_sY.charAt(i) - '0') * tenPow;
            tenPow = tenPow * 10;
        }
        sYear2 = sYear2 * 100;
        sYear1 = sYear2 - 99;
    }
    else if(str_sY.charAt(str_sY.length - 1) == 's' || str_sY.charAt(str_sY.length - 1) == 'S'){
        sYear1 = (str_sY.charAt(0) - '0') * 1000 + (str_sY.charAt(1) - '0') * 100 + (str_sY.charAt(2) - '0') * 10;
        sYear2 = sYear1 + 9;
    }


    if(yearNumCnt > 1){
        str_eY = words[yearNumInd[1]];

        if(onlyNum(str_eY) == true ){
            eYear1 = parseInt(str_eY);
            var tmpEY = eYear1;

            if(eYear1 < sYear2){
                if(tmpEY < 10){
                    eYear1 = eYear1 + sYear2 -(sYear2 % 10);
                    if(eYear1 < sYear2) eYear1 = eYear1 + 10;
                }
                else if(tmpEY < 100){
                    eYear1 = eYear1 + sYear2 -(sYear2 % 100);
                    if(eYear1 < sYear2) eYear1 = eYear1 + 100;
                }
                else if(tmpEY < 1000){
                    eYear1 = eYear1 + sYear2 -(sYear2 % 1000);
                    if(eYear1 < sYear2) eYear1 = eYear1 + 1000;
                }
            }
            eYear2 = eYear1;

        }
        else if(str_eY.charAt(str_eY.length - 2) == 't' || str_eY.charAt(str_eY.length - 2) == 'T'){
            var tenPow = 1;
            eYear2 = 0;
            for(var i = str_eY.length - 3; i >= 0; i--){
                eYear2 = eYear2 + (str_eY.charAt(i) - '0') * tenPow;
                tenPow = tenPow * 10;
            }
            eYear2 = eYear2 * 100;
            eYear1 = eYear2 - 99;
        }
        else if(str_eY.charAt(str_eY.length - 1) == 's' || str_eY.charAt(str_eY.length - 1) == 'S'){
            eYear1 = (str_eY.charAt(0) - '0') * 1000 + (str_eY.charAt(1) - '0') * 100 + (str_eY.charAt(2) - '0') * 10;
            eYear2 = eYear1 + 9;
        }
    }

    // console.log('syear - eyear');
    // console.log(sYear1, sYear2);
    // console.log(eYear1, eYear2);


    // detailed 1st year (e.g. 1st half, 2nd quarter...)

    var isLater = false;
    for(var i = 0; i < word_cnt; i++){
        if(words[i].toLowerCase() == "later"){
            isLater = true;
            break;
        }
    }

    if(isLater == true){
        startYear = sYear1;
        endYear = sYear2;
        if(yearNumCnt > 1) endYear = eYear2;
        endYear = endYear + yearCnt['later'][0];

        return {'start' : startYear , 'end' : endYear};
    }


    var periodFlag = -1;
    for(var i = 0; i < yearNumInd[0]; i++){
        periodFlag = isDQH(words[i]);
        if(periodFlag > -1) break;
    }

    // System.out.println(periodFlag);

    if(periodFlag >= 0 && periodFlag < 3){          // decade, quarter, half
        var order = [];
        var isLast = false;
        for(var i = 0; i < 10; i++) order[i] = false;
        for(var i = 0; i < yearNumInd[0]; i++){
            if((words[i].toLowerCase() == "1st") || (words[i].toLowerCase() == "first") ||
               (words[i].toLowerCase() == "1th")){
                order[0] = true;
            }else if(words[i].toLowerCase() == "2nd"|| words[i].toLowerCase() == "second"||
               words[i].toLowerCase() == "2th"){
                order[1] = true;
            }else if(words[i].toLowerCase() == "3rd" || words[i].toLowerCase() == "third" ||
               words[i].toLowerCase() == "3th"){
                order[2] = true;
            }else if(words[i].toLowerCase().includes("th") == true ){
                var strTmpOrder = words[i].substring(0, words[i].length - 2);
                if(onlyNum(strTmpOrder) == true ){
                    order[parseInt(strTmpOrder) - 1] = true;
                }
            }
            else if(words[i].toLowerCase() == "last"){
                isLast = true;
            }
        }

        if(isLast == true){
            if(periodFlag == 0) order[9] = true;
            else if(periodFlag == 1) order[3] = true;
            else order[1] = true;
        }

        var firstOrder = 0;
        var lastOrder = 0;

        for(firstOrder = 0; firstOrder < 10; firstOrder++){
            if(order[firstOrder] == true) break;
        }

        for(lastOrder = 9; lastOrder >= 0; lastOrder--){
            if(order[lastOrder] == true) break;
        }
        if(periodFlag == 0){
            sYear2 = sYear1 + (lastOrder + 1) * 10 - 1;
            sYear1 = sYear1 + firstOrder * 10;
        }else if(periodFlag == 1){
            sYear2 = sYear1 + (lastOrder + 1) * 25 - 1;
            sYear1 = sYear1 + firstOrder * 25;
        }else{
            sYear2 = sYear1 + (lastOrder + 1) * 50 - 1;
            sYear1 = sYear1 + firstOrder * 50;
        }
    }

    else if(periodFlag < 6 && periodFlag >= 3){     // early, mid, late
        var tmpY = sYear1;

        if(periodFlag == 3){                            // early century & XXXXs
            if(sYear2 - tmpY == 99){                            // early century
                sYear1 = tmpY + yearCnt['early'][0];
                sYear2 = tmpY + yearCnt['early'][1];
            }
            else if(sYear2 - tmpY == 9){                        // early XXXXs
                sYear1 = tmpY + yearCnt['searly'][0];
                sYear2 = tmpY + yearCnt['searly'][1];
            }
        }
        else if(periodFlag == 4){                       // mid century & XXXXs
            if(sYear2 - tmpY == 99){                            // mid century
                sYear1 = tmpY + yearCnt['mid'][0];
                sYear2 = tmpY + yearCnt['mid'][1];
            }
            else if(sYear2 - tmpY == 9){                        // mid XXXXs
                sYear1 = tmpY + yearCnt['smid'][0];
                sYear2 = tmpY + yearCnt['smid'][1];
            }
        }
        else if(periodFlag == 5){                       // late century & XXXXs
            if(sYear2 - tmpY == 99){                            // late century
                sYear1 = tmpY + yearCnt['late'][0];
                sYear2 = tmpY + yearCnt['late'][1];
            }
            else if(sYear2 - tmpY == 9){                        // late XXXXs
                sYear1 = tmpY + yearCnt['slate'][0];
                sYear2 = tmpY + yearCnt['slate'][1];
            }
        }
    }

    else if(periodFlag >= 6 && periodFlag <= 9){     // before, after, near, ca(c)
        if(periodFlag == 6){                                // before
            sYear2 = sYear1;
            sYear1 = sYear2 - yearCnt['before'][0];
        }
        else if(periodFlag == 7){                           // after
            sYear1 = sYear2;
            sYear2 = sYear1 + yearCnt['after'][0];
        }
        else if(periodFlag == 8){                           // near
            sYear1 = sYear1 - yearCnt['near'][0];
            sYear2 = sYear2 + yearCnt['near'][0];
        }
        else{                                               // ca, c
            sYear1 = sYear1 - yearCnt['ca'][0];
            sYear2 = sYear2 + yearCnt['ca'][0];
        }
    }

    else if(periodFlag == 10){
        var tmpY = sYear1;
        if(sYear2 - tmpY == 99){
            sYear1 = tmpY + yearCnt['end'][0];
            sYear2 = tmpY + yearCnt['end'][1];
        }
        else if(sYear2 - tmpY == 9){
            sYear1 = tmpY + yearCnt['send'][0];
            sYear2 = tmpY + yearCnt['send'][1];
        }
    }
    // detailed 2ns year (e.g. 1st half, 2nd quarter...)

    if(yearNumCnt > 1){
        var eIsLast = false;
        periodFlag = -1;
        for(var i = yearNumInd[0] + 1; i < yearNumInd[1]; i++){
            periodFlag = isDQH(words[i]);
            if(periodFlag > -1) break;
        }

        //System.out.println(periodFlag);

        if(periodFlag >= 0 && periodFlag < 3){
            var eorder = [];
            for(var i = 0; i < 10; i++) eorder[i] = false;
            for(var i = yearNumInd[0] + 1; i < yearNumInd[1]; i++){
                if((words[i].toLowerCase() == "1st") || (words[i].toLowerCase()== "first") ||
                   (words[i].toLowerCase() == "1th")){
                    eorder[0] = true;
                }else if(words[i].toLowerCase() == "2nd"|| words[i].toLowerCase() == "second"||
                   words[i].toLowerCase() == "2th"){
                    eorder[1] = true;
                }else if(words[i].toLowerCase() == "3rd" || words[i].toLowerCase() == "third" ||
                   words[i].toLowerCase() == "3th"){
                    eorder[2] = true;
                }else if(words[i].toLowerCase().includes("th") == true ){
                    var strTmpOrder = words[i].substring(0, words[i].length - 2);
                    if(onlyNum(strTmpOrder) == true ){
                        eorder[parseInt(strTmpOrder) - 1] = true;
                    }
                }
                else if(words[i].toLowerCase() == "last"){
                    eIsLast = true;
                }
            }

            if(eIsLast == true){
                if(periodFlag == 0) eorder[9] = true;
                else if(periodFlag == 1) eorder[3] = true;
                else eorder[1] = true;
            }

            var firstOrder = 0;
            var lastOrder = 0;

            for(firstOrder = 0; firstOrder < 10; firstOrder++){
                if(eorder[firstOrder] == true) break;
            }

            for(lastOrder = 9; lastOrder >= 0; lastOrder--){
                if(eorder[lastOrder] == true) break;
            }

            if(periodFlag == 0){
                eYear2 = eYear1 + (lastOrder + 1) * 10 - 1;
                eYear1 = eYear1 + firstOrder * 10;
            }else if(periodFlag == 1){
                eYear2 = eYear1 + (lastOrder + 1) * 25 - 1;
                eYear1 = eYear1 + firstOrder * 25;
            }else{
                eYear2 = eYear1 + (lastOrder + 1) * 50 - 1;
                eYear1 = eYear1 + firstOrder * 50;
            }
        }

        else if(periodFlag < 6 && periodFlag >= 3){     // early, mid, late

            var tmpY = eYear1;

            if(periodFlag == 3){                            //early century & XXXXs
                if(eYear2 - tmpY == 99){                            // early century
                    eYear1 = tmpY + yearCnt['early'][0];
                    eYear2 = tmpY + yearCnt['early'][1];
                }
                else if(eYear2 - tmpY == 9){                        // early XXXXs
                    eYear1 = tmpY + yearCnt['searly'][0];
                    eYear2 = tmpY + yearCnt['searly'][1];
                }
            }

            else if(periodFlag == 4){                       // mid century & XXXXs
                if(eYear2 - tmpY == 99){                            // mid century
                    eYear1 = tmpY + yearCnt['mid'][0];
                    eYear2 = tmpY + yearCnt['mid'][1];
                }
                else if(eYear2 - tmpY == 9){                        // mid XXXXs
                    eYear1 = tmpY + yearCnt['smid'][0];
                    eYear2 = tmpY + yearCnt['smid'][1];
                }
            }

            else if(periodFlag == 5){                       // late century & XXXXs
                if(eYear2 - tmpY == 99){                            // late century
                    eYear1 = tmpY + yearCnt['late'][0];
                    eYear2 = tmpY + yearCnt['late'][1];
                }
                else if(eYear2 - tmpY == 9){                        // late XXXXs
                    eYear1 = tmpY + yearCnt['slate'][0];
                    eYear2 = tmpY + yearCnt['slate'][1];
                }
            }
        }

        else if(periodFlag >= 6 && periodFlag <= 9){     // before, after, near
            if(periodFlag == 6){                                // before
                eYear2 = eYear1;
                eYear1 = eYear2 - yearCnt['before'][0];
            }
            else if(periodFlag == 7){                           // after
                eYear1 = eYear2;
                eYear2 = eYear1 + yearCnt['after'][0];
            }
            else if(periodFlag == 8){                           // near
                eYear1 = eYear1 - yearCnt['near'][0];
                eYear2 = eYear2 + yearCnt['near'][0];
            }
            else{                                               // ca, c
                eYear1 = eYear1 - yearCnt['ca'][0];
                eYear2 = eYear2 + yearCnt['ca'][0];
            }
        }

        else if(periodFlag == 10){
            var tmpY = eYear1;
            if(eYear2 - tmpY == 99){
                eYear1 = tmpY + yearCnt['end'][0];
                eYear2 = tmpY + yearCnt['end'][1];
            }
            else if(eYear2 - tmpY == 9){
                eYear1 = tmpY + yearCnt['send'][0];
                eYear2 = tmpY + yearCnt['send'][1];
            }
        }

    }

    //System.out.println(sYear1 + " - " + sYear2 + " , " + eYear1 + " - " + eYear2);

    var flag = false;

    for(var i = 0; i < yearNumInd[0]; i++){
        if(words[i].toLowerCase() == "between" || words[i].toLowerCase() == "signed" ||
           words[i].toLowerCase() == "formerly" || words[0].toLowerCase() == "inscribed" ||
           words[i].toLowerCase() == "dated"){
            flag = true;
            break;
        }
    }

    startYear = sYear1;
    endYear = sYear2;
    if(yearNumCnt > 1) endYear = eYear2;
    // console.log(startYear + " - " + endYear);

    return {'start' : startYear , 'end' : endYear};
}

function junkCharRemove(str){
    var removedStr = "";

    for(var i = 0; i < str.length; i++){
        var tmpC = str.charAt(i);
        if(tmpC <= '9' && tmpC >= '0'){
            removedStr = removedStr + tmpC;
            continue;
        }
        if(tmpC <= 'z' && tmpC >= 'a'){
            removedStr = removedStr + tmpC;
            continue;
        }
        if(tmpC <= 'Z' && tmpC >= 'A'){
            removedStr = removedStr + tmpC;
            continue;
        }
        removedStr = removedStr + " ";
    }


    var words = [];
    words = removedStr.split(" ");

    var rltStr = words[0];
    for(var i = 1; i < words.length; i++){
        if(words[i].includes("quarter") == true){
            rltStr = rltStr + " quarter";
        }
        else if(words[i].includes("half") == true){
            rltStr = rltStr + " half";
        }
        else if(words[i].includes("decade") == true){
            rltStr = rltStr + " decade";
        }
        else{
            rltStr = rltStr + " " + words[i];
        }
    }

    return rltStr;
}

function trainWords(oriStr, sFrom, sTo){

    var str = junkCharRemove(oriStr);

    var from = parseInt(sFrom);
    var to = parseInt(sTo);

    var words = [];
    words = str.split(" ");

    var isValid = false;
    var wordInd = 0;
    var isXXXXS = false;

    for(var i = 0; i < words.length; i++){                  // if year is century or XXXXs
        if(words[i].length < 5) continue;
        if(words[i].charAt(4).toLowerCase() != 's') continue;
        for(var j = 0; j < words[i].length - 1; j++){
            if(words[i].charAt(j) < '0' || words[i].charAt(j) > '9') break;
        }
        if(j < words[i].length - 1) continue;

        isXXXXS = true;
    }

    for(var i = 0; i < words.length; i++){
        if(isDQH(words[i]) > 2 || words[i].toLowerCase() == 'later'){
            wordInd = i;
            isValid = true;
            break;
        }
    }

    var isDouble = 0;

    if(isValid == true){
        var inWord = words[wordInd];

        if(isXXXXS == true){
            inWord = "s" + inWord;
        }

        // console.log("inWord : " + inWord + " : " + trainFlag[inWord]);

        isDouble = trainFlag[inWord];

        if(isDouble == 0){
            trainFlag[inWord] = 1;

            yearCnt[inWord][0] = from;
            // console.log("from : " + yearCnt[inWord][0]);

            var tmpDQH = isDQH(words[wordInd]);
            if( tmpDQH < 6 || tmpDQH == 10){
                yearCnt[inWord][1] = to;
                // console.log("to : " + yearCnt[inWord][1]);
            }
        }
    }

    return { 'result' : isDouble };
}
